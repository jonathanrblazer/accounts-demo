# FastAPI, MongoDB, and React

This is an example application that shows how to build a Web
application with FastAPI, MongoDB, React, and the React
Toolkit.

## Running the application

1. Clone this repo.
1. Change your working directory to the cloned repo's
   directory.
1. Run the following commands:

    ```sh
    docker volume create example-node-modules
    docker volume create example-mongo-data
    docker compose build
    docker compose up
    ```

## View OpenAPI documentation

Access the docs at:

* Accounts API: <http://localhost:8000/docs>!
* Books API: <http://localhost:8001/docs>!

## Logging in

Access the GHI at <http://localhost:3000>.

The example data contains two logins for you to use with
different permissions.

| Email                   | Password   |
|-------------------------|------------|
| `librarian@example.com` | `password` |
| `patron@example.com`    | `password` |

## Playing with WebSockets

While you have the application open in a normal browser
window, open another browser window in Incognito Mode.
Access the GHI at <http://localhost:3000>.

In the normal window, make sure you're logged in. Then,
borrow or return a book. You can see the numbers change in
the Incognito window, too!

## Seeing the data

You can see the data in MongoDB in the `library` database.
You should be able to access the documents at
<http://localhost:8081/db/library/>.
