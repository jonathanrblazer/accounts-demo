import { useNavigate, useParams } from 'react-router-dom';
import { preventDefault } from './app/utils';
import { useAddBookMutation } from './app/api';


function BookForm() {
  const [ addBook, { data } ] = useAddBookMutation();
  const { bookId = "create" } = useParams();
  const navigate = useNavigate();

  if (data) {
    navigate("/");
  }

  return (
    <div className="box">
      <div className="content">
        <h1>Book {bookId}</h1>
        <form method="post" action="/" onSubmit={preventDefault(addBook, e => e.target)}>
        <div className="field">
            <label className="label">Title</label>
            <div className="control">
              <input name="title" required className="input" type="text" placeholder="Fahrenheit 451" />
            </div>
          </div>
          <div className="field">
            <label className="label">Author (Last name, first name)</label>
            <div className="control">
              <input name="author" required className="input" type="text" placeholder="Bradbury, Ray" />
            </div>
          </div>
          <div className="field">
            <label className="label">Quantity</label>
            <div className="control">
              <input name="quantity" required className="input" type="number" min="1" placeholder="10" />
            </div>
          </div>
          <div className="field">
            <label className="label">Synopsis</label>
            <div className="control">
              <textarea name="synopsis" required className="textarea" placeholder="You won't believe this story..." />
            </div>
          </div>
          <div className="field">
            <label className="label">Book cover</label>
            <div className="control">
              <input name="cover" className="input" type="url" placeholder="https://some.site/cover.png" />
            </div>
          </div>
          <div className="field is-grouped">
            <div className="control">
              <button className="button is-primary">Create</button>
            </div>
            <div className="control">
              <button onClick={() => navigate("/")} type="button" className="button">Cancel</button>
            </div>
          </div>
        </form>
      </div>
    </div>
  );
}

export default BookForm;
